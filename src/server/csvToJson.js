const csvtojson = require('csvtojson');
const fs = require('fs');


const csvFilePath = "../data/matches.csv"

csvtojson()
    .fromFile(csvFilePath)
    .then((json) => {
        console.log(json);

        fs.writeFileSync('matches.json', JSON.stringify(json), "utf-8", (err) => {
            if (err) console.log(err);
        })

    })

const csvFilePathTwo = "../data/deliveries.csv"

csvtojson()
    .fromFile(csvFilePathTwo)
    .then((json) => {
        console.log(json);

        fs.writeFileSync('deliveries.json', JSON.stringify(json), "utf-8", (err) => {
            if (err) console.log(err);
        })

    })