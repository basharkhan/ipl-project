const matches = require('./matches.json');
const deliveries = require('./deliveries.json');

//  1. Number of matches played per year for all the years in IPL.

let matchesPerYear = matches.reduce((acc, cur) => {

    if (acc[cur.season]) {
        acc[cur.season] = ++acc[cur.season];
    }
    else {
        acc[cur.season] = 1;
    }

    return acc;

}, {})

module.exports = matchesPerYear;
